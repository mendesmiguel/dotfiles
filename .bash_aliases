alias manage='python $VIRTUAL_ENV/../manage.py'
alias config='/usr/bin/git --git-dir=$HOME/.dotcfg/ --work-tree=$HOME'
alias g=git
alias jnote='jupyter notebook'
