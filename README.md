# Dotfiles for [@miguendes](http://twitter.com/miguendes)

## Requirements

- Git
- Curl

## Install

Install config tracking in your $HOME by running:

  curl -Lks https://gitlab.com/mendesmiguel/dotfiles/raw/master/.bin/cfg-install.sh | /bin/bash

## References
   
Based on https://news.ycombinator.com/item?id=11070797 and https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/